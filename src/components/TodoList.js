import React from 'react';
import { observer } from 'mobx-react'

@observer
class App extends React.Component {
  render() {
    return (
        <h1>MObx{this.props.store.todos[0]}</h1>
    )
  }
}

export default App;
