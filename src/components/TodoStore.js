import { decorate, autorun, observable } from 'mobx'
import {observer} from "mobx-react"

class TodoStore {
    @observable todos = ['buy milk','buy eggs']
    @observable filter = ''
}

var store = window.store = new TodoStore

export default store

autorun(()=>{
    console.log(store.filter)
    console.log(store.todo[0])
})